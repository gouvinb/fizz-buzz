import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.AndroidBaselicationInfo
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("app.gouvinb.build-logic.project-information")
	id("app.gouvinb.build-logic.project-configuration")

	id("com.android.library")
	kotlin("android")
}

tasks.withType<KotlinCompile>().all {
	kotlinOptions {
		jvmTarget = AndroidBaselicationInfo.jvmTarget
	}
}

dependencies {}