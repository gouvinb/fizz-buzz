package app.gouvinb.library.utils

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
 */
sealed class State<out T : Any> {

	data class Valid<out T : Any>(val data: T) : State<T>()
	data class Invalid(val exception: Exception) : State<Nothing>()

	override fun toString(): String {
		return when (this) {
			is Valid<*> -> "Valid[data=$data]"
			is Invalid -> "Invalid[exception=$exception]"
		}
	}
}