plugins {
	id("app.gouvinb.build-logic.project-information")
}

// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
	repositories {
		google()
		gradlePluginPortal()
		mavenCentral()
	}
	dependencies {
		classpath("com.android.tools.build:gradle:7.0.4")
		classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31")

		// NOTE: Do not place your application dependencies here; they belong
		// in the individual module build.gradle files
	}
}

allprojects {
	repositories {
		google()
		gradlePluginPortal()
		mavenCentral()
	}
}

tasks.register("clean", type = Delete::class) {
	destroyables.register(rootProject.buildDir)
	doLast { delete(rootProject.buildDir) }
}