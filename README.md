# Fizz Buzz (édition entreprise)

## Objectif

L'objectif est d'avoir un projet basique avec un formulaire pour paramétrer un
fizz-buzz, puis d'ouvrir un autre écran avec une liste de nombres dont certains
sont remplacés par Fizz/Buzz/FizzBuzz.

Ce projet est pensé pour offrir un maximum de liberté sur le design et le
déploiement sans impacter les performances de l'application et de la
productivité du développeur.

## Module `build-logic`

Ce module est une intégration à notre configuration de Gradle (
ou `composite build`) afin de standardiser les modules Gradle. Il est composé de
2 principaux plugins :

1. **ProjectInformationPlugin** : Sert à lister une série de variable
   réutilisable ainsi que l'ensemble des dépendances du projet afin de ne pas
   avoir de conflit de version ou de configuration entre les modules.
2. **ProjectConfigurationPlugin** : Applique certaines configurations en
   fonction des plugins ajoutés au module courant.

### Pourquoi je n'ai pas utilisé `BuildSrc` à la place ?

Lire [cet article](
https://medium.com/bumble-tech/how-to-use-composite-builds-as-a-replacement-of-buildsrc-in-gradle-64ff99344b58)
pour en savoir plus.

## Les modules `library-utils` et `feature-fizzbuzz`

Le choix de séparer le code fonctionnel de l'application s'explique sur
plusieurs points :

1. Cela donne la possibilité de refaire le design de l'application sans se
   soucier du fonctionnel (pratique pour les refontes graphiques).
2. Cela permet au système de génération de compiler uniquement les modules que
   l'on modifie et de mettre en cache ces sorties pour les futures générations.
3. Cela rend également l'exécution de projets parallèles plus efficace.

### Module `library-utils`

Ce module centralise un ensemble de méthodes qui ne compte pas changer sur le
long terme, il sert principalement à gagner du temps de compilation.

Dans ce projet, la classe `State` peut être réutilisée potentiellement dans
d'autre module ou d'autres cas et l'extension `EditText.afterTextChanged`
peut potentiellement ne jamais changer, car son but reste très simple.

### Module `feature-fizzbuzz`

Ce module contient la partie fonctionnelle du fizz-buzz.

* Il offre un accès sécurisé pour paramétrer le fizz-buzz.
* Pour des soucis de performance le fizz-buzz n'est pas calculé entièrement, il
  va calculer uniquement un entier souhaitez, l'utilisateur peut demander une
  liste de `2^31-1` éléments sans déclencher d'`OOM` (à condition que l'UI soit
  bien codé !).

## Module `app`

Ce module contient uniquement l'UI de l'application et utilise les modules
`library-utils` et `feature-fizzbuzz`.

La première Activity est le formulaire permettant de paramétrer le fizz-buzz.
Elle vérifie la validité des paramètres à chaque changement en passant par
le `FizzBuzzViewModel`. Elle utilise uniquement le `viewBinding` pour facilité
l'écriture.

La deuxième Activity est l'affichage en liste du fizz-buzz. Chaque élément de
cette liste va être traité à la volée avant d'être affiché sur l'appareil. Elle
utilise jetpack compose pour la démonstration. Dans les grandes lignes, chaque
élément de cette liste peut être recyclés comme le ferai un
`RecyclerView` afin de garder une expérience correct et de ne pas utiliser trop
de ressource au chargement de la liste.

## Time tracking

Voir [le fichier org](./TIME_TRACKING.org).

Ce document concerne uniquement le premier commit du projet.

## Ce que j'améliorerais si j'avais plus de temps

### Projet (1ère partie)

1. Je complèterai la documentation
2. J'ajouterai detekt
3. Je configurerai un flux de travail d'intégration continue

### Application

1. Je migrerai `FizzBuzzFormActivity` vers jetpack compose
2. Je complèterai les tests d'instrumentation
3. J'améliorerai la gestion d'erreur pour que l'utilisateur final puisse mieux
   comprendre ses erreurs de saisies.
4. Je ~~re~~ferai le design de l'application
5. J'ajouterai rapporteur de crash et le formulaire RGPD qui va avec

### Projet (2ème partie)

1. J'ajouterai GPP (gradle play publisher)
2. Je configurerai un flux de travail de déploiement continu
3. Je déploierai l'application sur le Google Play Store avec une phase d'Alpha
   fermé, une phase Beta ouverte puis la Production grand-publique
