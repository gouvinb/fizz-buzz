rootProject.name = "Fizz Buzz"

includeBuild("build-logic")

include(":app")
include(":library-utils")
include(":feature-fizzbuzz")
