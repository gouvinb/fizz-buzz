package app.gouvinb.feature.fizzbuzz.ui.model

import app.gouvinb.feature.fizzbuzz.R
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzFormState.Companion.fromRawData
import org.junit.Assert.assertEquals
import org.junit.Test

class FizzBuzzFormStateTest {

	@Test
	fun `fromRawData valid`() {
		assertEquals(FizzBuzzFormState(null, null, null, true), fromRawData("1", "1", "1"))
	}

	@Test
	fun `fromRawData invalid`() {
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, null, false), fromRawData("0", "1", "1"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, null, false), fromRawData("a", "1", "1"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, null, false), fromRawData("", "1", "1"))

		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, null, false), fromRawData("1", "0", "1"))
		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, null, false), fromRawData("1", "a", "1"))
		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, null, false), fromRawData("1", "", "1"))

		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, null, false), fromRawData("0", "0", "1"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, null, false), fromRawData("a", "a", "1"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, null, false), fromRawData("", "", "1"))

		assertEquals(FizzBuzzFormState(null, null, R.string.invalid_limit, false), fromRawData("1", "1", "0"))
		assertEquals(FizzBuzzFormState(null, null, R.string.invalid_limit, false), fromRawData("1", "1", "a"))
		assertEquals(FizzBuzzFormState(null, null, R.string.invalid_limit, false), fromRawData("1", "1", ""))

		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, R.string.invalid_limit, false), fromRawData("0", "1", "0"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, R.string.invalid_limit, false), fromRawData("a", "1", "a"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, null, R.string.invalid_limit, false), fromRawData("", "1", ""))

		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("1", "0", "0"))
		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("1", "a", "a"))
		assertEquals(FizzBuzzFormState(null, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("1", "", ""))

		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("0", "0", "0"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("a", "a", "a"))
		assertEquals(FizzBuzzFormState(R.string.invalid_multiple_fizz, R.string.invalid_multiple_buzz, R.string.invalid_limit, false), fromRawData("", "", ""))
	}
}
