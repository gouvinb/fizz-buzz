package app.gouvinb.feature.fizzbuzz.data

import app.gouvinb.feature.fizzbuzz.data.model.FizzBuzzData
import app.gouvinb.library.utils.State
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class FizzBuzzDataSourceTest {

	private lateinit var dataSource: FizzBuzzDataSource

	@Before
	fun `init FizzBuzzDataSource`() {
		dataSource = FizzBuzzDataSource()
	}

	@Test
	fun `checkFizzBuzzForm return a State$Valid`() {
		assertEquals(State.Valid(FizzBuzzData(1, 1, 1)), dataSource.checkFizzBuzzForm("1", "1", "1"))
	}

	@Test
	fun `checkFizzBuzzForm return a State$Invalid`() {
		val invalidMultiple1WithBadInt = dataSource.checkFizzBuzzForm("0", "1", "1")
		assertTrue(invalidMultiple1WithBadInt is State.Invalid)
		assertEquals((invalidMultiple1WithBadInt as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple1WithBadInt.exception.cause?.message, "`multiple1` must be  an Integer >= 1")

		val invalidMultiple1WithString = dataSource.checkFizzBuzzForm("a", "1", "1")
		assertTrue(invalidMultiple1WithString is State.Invalid)
		assertEquals((invalidMultiple1WithString as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple1WithString.exception.cause?.message, "`multiple1` must be  an Integer >= 1")

		val invalidMultiple1WithEmptyInput = dataSource.checkFizzBuzzForm("", "1", "1")
		assertTrue(invalidMultiple1WithEmptyInput is State.Invalid)
		assertEquals((invalidMultiple1WithEmptyInput as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple1WithEmptyInput.exception.cause?.message, "`multiple1` must be  an Integer >= 1")

		val invalidMultiple2WithBadInt = dataSource.checkFizzBuzzForm("1", "0", "1")
		assertTrue(invalidMultiple2WithBadInt is State.Invalid)
		assertEquals((invalidMultiple2WithBadInt as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple2WithBadInt.exception.cause?.message, "`multiple2` must be  an Integer >= 1")

		val invalidMultiple2WithString = dataSource.checkFizzBuzzForm("1", "a", "1")
		assertTrue(invalidMultiple2WithString is State.Invalid)
		assertEquals((invalidMultiple2WithString as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple2WithString.exception.cause?.message, "`multiple2` must be  an Integer >= 1")

		val invalidMultiple2WithEmptyInput = dataSource.checkFizzBuzzForm("1", "", "1")
		assertTrue(invalidMultiple2WithEmptyInput is State.Invalid)
		assertEquals((invalidMultiple2WithEmptyInput as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidMultiple2WithEmptyInput.exception.cause?.message, "`multiple2` must be  an Integer >= 1")

		val invalidLimitWithBadInt = dataSource.checkFizzBuzzForm("1", "1", "0")
		assertTrue(invalidLimitWithBadInt is State.Invalid)
		assertEquals((invalidLimitWithBadInt as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidLimitWithBadInt.exception.cause?.message, "`limit` must be  an Integer >= 1")

		val invalidLimitWithString = dataSource.checkFizzBuzzForm("1", "1", "a")
		assertTrue(invalidLimitWithString is State.Invalid)
		assertEquals((invalidLimitWithString as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidLimitWithString.exception.cause?.message, "`limit` must be  an Integer >= 1")

		val invalidLimitWithEmptyInput = dataSource.checkFizzBuzzForm("1", "1", "")
		assertTrue(invalidLimitWithEmptyInput is State.Invalid)
		assertEquals((invalidLimitWithEmptyInput as State.Invalid).exception.message, "Invalid input")
		assertEquals(invalidLimitWithEmptyInput.exception.cause?.message, "`limit` must be  an Integer >= 1")
	}
}