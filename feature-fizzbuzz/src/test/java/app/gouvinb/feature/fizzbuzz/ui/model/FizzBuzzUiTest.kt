package app.gouvinb.feature.fizzbuzz.ui.model

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

/**
 * User details post authentication that is exposed to the UI
 */
class FizzBuzzUiTest {
	private lateinit var fizzBuzzUi: FizzBuzzUi

	@Before
	fun `init FizzBuzzUi`() {
		fizzBuzzUi = FizzBuzzUi(2, 3, 1)
	}

	@Test
	fun `FizzBuzzUi$parseInt test`() {
		assertEquals(fizzBuzzUi.parseInt(0), FIZZ_BUZZ)
		assertEquals(fizzBuzzUi.parseInt(1), "1")
		assertEquals(fizzBuzzUi.parseInt(2), FIZZ)
		assertEquals(fizzBuzzUi.parseInt(3), BUZZ)
		assertEquals(fizzBuzzUi.parseInt(4), FIZZ)
		assertEquals(fizzBuzzUi.parseInt(5), "5")
		assertEquals(fizzBuzzUi.parseInt(6), FIZZ_BUZZ)
	}

	companion object {
		const val FIZZ = "Fizz"
		const val BUZZ = "Buzz"
		const val FIZZ_BUZZ = "$FIZZ$BUZZ"
	}
}