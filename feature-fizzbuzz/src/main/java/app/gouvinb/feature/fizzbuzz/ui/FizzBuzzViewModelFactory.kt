package app.gouvinb.feature.fizzbuzz.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import app.gouvinb.feature.fizzbuzz.data.FizzBuzzDataSource
import app.gouvinb.feature.fizzbuzz.data.FizzBuzzRepository

/**
 * ViewModel provider factory to instantiate FizzBuzzViewModel.
 * Required given FizzBuzzViewModel has a non-empty constructor
 */
class FizzBuzzViewModelFactory : ViewModelProvider.Factory {

	@Suppress("UNCHECKED_CAST")
	override fun <T : ViewModel> create(modelClass: Class<T>): T {
		if (modelClass.isAssignableFrom(FizzBuzzViewModel::class.java)) {
			return FizzBuzzViewModel(
				fizzBuzzRepository = FizzBuzzRepository(
					dataSource = FizzBuzzDataSource()
				)
			) as T
		}
		throw IllegalArgumentException("Unknown ViewModel class")
	}
}