package app.gouvinb.feature.fizzbuzz.ui.model

import java.io.Serializable

/**
 * User details post authentication that is exposed to the UI
 */
data class FizzBuzzUi(
	val multiple1: Int,
	val multiple2: Int,
	val rangeLimit: Int,
) : Serializable {
	fun parseInt(int: Int): String {
		val isMultiple1 = int % multiple1 == 0
		val isMultiple2 = int % multiple2 == 0
		return when {
			isMultiple1 && isMultiple2 -> "FizzBuzz"
			isMultiple1 -> "Fizz"
			isMultiple2 -> "Buzz"
			else -> int.toString()
		}
	}
}