package app.gouvinb.feature.fizzbuzz.ui.model

/**
 * Authentication result : success (fizzBuzzData details) or error message.
 */
data class FizzBuzzResult(
	val success: FizzBuzzUi? = null,
	val error: Int? = null,
)