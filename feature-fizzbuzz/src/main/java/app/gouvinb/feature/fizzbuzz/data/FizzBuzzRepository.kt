package app.gouvinb.feature.fizzbuzz.data

import app.gouvinb.feature.fizzbuzz.data.model.FizzBuzzData
import app.gouvinb.library.utils.State

/**
 * Class that check raw data and fizzBuzzData from the data source and
 * maintains an in-memory cache of fizzBuzzData.
 */
class FizzBuzzRepository(val dataSource: FizzBuzzDataSource) {

	// in-memory cache of the FizzBuzzData object
	var fizzBuzzData: FizzBuzzData? = null
		private set

	fun check(multiple1Str: String, multiple2Str: String, limitStr: String): State<FizzBuzzData> =
		dataSource.checkFizzBuzzForm(multiple1Str, multiple2Str, limitStr).also {
			if (it is State.Valid) setValidData(it.data)
		}

	private fun setValidData(data: FizzBuzzData) {
		this.fizzBuzzData = data
	}
}