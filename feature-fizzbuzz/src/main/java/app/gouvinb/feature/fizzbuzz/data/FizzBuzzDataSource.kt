package app.gouvinb.feature.fizzbuzz.data

import app.gouvinb.feature.fizzbuzz.data.model.FizzBuzzData
import app.gouvinb.library.utils.State
import java.io.IOException

/**
 * Class that handles check and retrieves fizzBuzzData information.
 */
class FizzBuzzDataSource {
	fun checkFizzBuzzForm(multiple1Str: String, multiple2Str: String, limitStr: String) = try {
		val multiple1 = if (!isMultipleValid(multiple1Str)) throw Throwable("`multiple1` must be  an Integer >= 1") else multiple1Str.toInt()
		val multiple2 = if (!isMultipleValid(multiple2Str)) throw Throwable("`multiple2` must be  an Integer >= 1") else multiple2Str.toInt()
		val limit = if (!isLimitValid(limitStr)) throw Throwable("`limit` must be  an Integer >= 1") else limitStr.toInt()
		State.Valid(FizzBuzzData(multiple1, multiple2, limit))
	} catch (e: Throwable) {
		State.Invalid(IOException("Invalid input", e))
	}

	// A placeholder username validation check
	private fun isMultipleValid(multipleStr: String) = multipleStr.toIntOrNull()?.let { it >= 1 } ?: false

	// A placeholder password validation check
	private fun isLimitValid(limitStr: String) = limitStr.toIntOrNull()?.let { it >= 1 } ?: false
}