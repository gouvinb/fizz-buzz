package app.gouvinb.feature.fizzbuzz.data.model

/**
 * Data class that captures fizzBuzzData information for logged in users retrieved from FizzBuzzRepository
 */
data class FizzBuzzData(
	val multiple1: Int,
	val multiple2: Int,
	val limit: Int,
)