package app.gouvinb.feature.fizzbuzz.ui.model

import androidx.annotation.StringRes
import app.gouvinb.feature.fizzbuzz.R

/**
 * Data validation state of the fizz-buzz form.
 */
data class FizzBuzzFormState(
	@StringRes var multiple1Error: Int? = null,
	@StringRes var multiple2Error: Int? = null,
	@StringRes var limitError: Int? = null,
	var isDataValid: Boolean = false,
) {
	companion object {
		fun fromRawData(multiple1Str: String, multiple2Str: String, limitStr: String): FizzBuzzFormState {
			return FizzBuzzFormState().apply {
				if (!isMultipleValid(multiple1Str)) multiple1Error = R.string.invalid_multiple_fizz
				if (!isMultipleValid(multiple2Str)) multiple2Error = R.string.invalid_multiple_buzz
				if (!isLimitValid(limitStr)) limitError = R.string.invalid_limit
				isDataValid = multiple1Error == null && multiple2Error == null && limitError == null
			}
		}

		// A placeholder Fizz/Buzz multiple validation check
		private fun isMultipleValid(multipleStr: String) = multipleStr.toIntOrNull()?.let { it >= 1 } ?: false

		// A placeholder rangeLimit limit validation check
		private fun isLimitValid(limitStr: String) = limitStr.toIntOrNull()?.let { it >= 1 } ?: false
	}
}