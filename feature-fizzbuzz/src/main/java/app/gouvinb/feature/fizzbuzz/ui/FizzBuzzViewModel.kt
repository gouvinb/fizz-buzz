package app.gouvinb.feature.fizzbuzz.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.gouvinb.feature.fizzbuzz.R
import app.gouvinb.feature.fizzbuzz.data.FizzBuzzRepository
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzFormState
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzResult
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzUi
import app.gouvinb.library.utils.State

class FizzBuzzViewModel(private val fizzBuzzRepository: FizzBuzzRepository) : ViewModel() {

	private val _fizzBuzzForm = MutableLiveData<FizzBuzzFormState>()
	val fizzBuzzFormState: LiveData<FizzBuzzFormState> = _fizzBuzzForm

	private val _fizzBuzzResult = MutableLiveData<FizzBuzzResult>()
	val fizzBuzzResult: LiveData<FizzBuzzResult> = _fizzBuzzResult

	fun submit(multiple1Str: String, multiple2Str: String, limitStr: String) {
		// can be launched in a separate asynchronous job
		val result = fizzBuzzRepository.check(multiple1Str, multiple2Str, limitStr)

		if (result is State.Valid) {
			val fizzBuzzData = result.data
			_fizzBuzzResult.value = FizzBuzzResult(success = FizzBuzzUi(fizzBuzzData.multiple1, fizzBuzzData.multiple2, fizzBuzzData.limit))
		} else {
			_fizzBuzzResult.value = FizzBuzzResult(error = R.string.bad_input_data)
		}
	}

	fun fizzBuzzDataChanged(multiple1Str: String, multiple2Str: String, limitStr: String) {
		_fizzBuzzForm.value = FizzBuzzFormState.fromRawData(multiple1Str, multiple2Str, limitStr)
	}
}