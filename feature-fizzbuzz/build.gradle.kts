import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.AndroidBaselicationInfo
import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.Dependencies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("app.gouvinb.build-logic.project-information")
	id("app.gouvinb.build-logic.project-configuration")

	id("com.android.library")
	kotlin("android")
}

tasks.withType<KotlinCompile>().all {
	kotlinOptions {
		jvmTarget = AndroidBaselicationInfo.jvmTarget
	}
}

dependencies {
	implementation(Dependencies.androidxAnnotationAnnotation)
	implementation(Dependencies.androidxCoreCoreKtx)
	implementation(Dependencies.androidxLifecycleLifecycleLiveDataKtx)
	implementation(Dependencies.androidxLifecycleLifecycleViewmodelKtx)


	testImplementation(Dependencies.junitJunit)
	androidTestImplementation(Dependencies.androidxTestExtJunitKtx)
	androidTestImplementation(Dependencies.androidxTestEspressoEspressoCore)
	implementation(project(":library-utils"))
}