package app.gouvinb.fizzbuzz

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import app.gouvinb.feature.fizzbuzz.ui.FizzBuzzViewModel
import app.gouvinb.feature.fizzbuzz.ui.FizzBuzzViewModelFactory
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzUi
import app.gouvinb.fizzbuzz.FizzBuzzActivity.Companion.FIZZ_BUZZ_UI_DATA
import app.gouvinb.fizzbuzz.databinding.ActivityFizzBuzzFormBinding
import app.gouvinb.library.utils.extension.afterTextChanged

class FizzBuzzFormActivity : AppCompatActivity() {

	private lateinit var fizzBuzzViewModel: FizzBuzzViewModel
	private lateinit var binding: ActivityFizzBuzzFormBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		binding = ActivityFizzBuzzFormBinding.inflate(layoutInflater)
		setContentView(binding.root)

		fizzBuzzViewModel = ViewModelProvider(this, FizzBuzzViewModelFactory())[FizzBuzzViewModel::class.java]

		fizzBuzzViewModel.fizzBuzzFormState.observe(this@FizzBuzzFormActivity, Observer { fizzBuzzFormState ->
			val fizzBuzzFormStateSafe = fizzBuzzFormState ?: return@Observer

			// disable submit button unless both data is valid
			binding.submit.isEnabled = fizzBuzzFormStateSafe.isDataValid

			fizzBuzzFormStateSafe.multiple1Error.takeIf { it != null }?.also { binding.multiple1Text.error = getString(it) }
			fizzBuzzFormStateSafe.multiple2Error.takeIf { it != null }?.also { binding.multiple2Text.error = getString(it) }
			fizzBuzzFormStateSafe.limitError.takeIf { it != null }?.also { binding.limitText.error = getString(it) }
		})

		fizzBuzzViewModel.fizzBuzzResult.observe(this@FizzBuzzFormActivity, Observer {
			val fizzBuzzResult = it ?: return@Observer

			binding.loading.visibility = View.GONE
			fizzBuzzResult.error?.also { error -> showError(error) }
			fizzBuzzResult.success?.also { success -> updateUiWithUser(success) }
			setResult(Activity.RESULT_OK)
		})

		binding.multiple1Text.afterTextChanged {
			fizzBuzzViewModel.fizzBuzzDataChanged(
				binding.multiple1Text.text.toString(),
				binding.multiple2Text.text.toString(),
				binding.limitText.text.toString(),
			)
		}

		binding.multiple2Text.afterTextChanged {
			fizzBuzzViewModel.fizzBuzzDataChanged(
				binding.multiple1Text.text.toString(),
				binding.multiple2Text.text.toString(),
				binding.limitText.text.toString(),
			)
		}

		binding.limitText.apply {
			afterTextChanged {
				fizzBuzzViewModel.fizzBuzzDataChanged(
					binding.multiple1Text.text.toString(),
					binding.multiple2Text.text.toString(),
					binding.limitText.text.toString(),
				)
			}

			setOnEditorActionListener { _, actionId, _ ->
				when (actionId) {
					EditorInfo.IME_ACTION_DONE ->
						fizzBuzzViewModel.submit(
							binding.multiple1Text.text.toString(),
							binding.multiple2Text.text.toString(),
							binding.limitText.text.toString(),
						)
				}
				false
			}
		}

		binding.submit.setOnClickListener {
			binding.loading.visibility = View.VISIBLE
			fizzBuzzViewModel.submit(
				binding.multiple1Text.text.toString(),
				binding.multiple2Text.text.toString(),
				binding.limitText.text.toString(),
			)
		}
	}

	private fun updateUiWithUser(model: FizzBuzzUi) {
		Toast.makeText(applicationContext, "Ok for $model", Toast.LENGTH_LONG).show()
		val intent = Intent(this, FizzBuzzActivity::class.java).apply {
			putExtra(FIZZ_BUZZ_UI_DATA, model)
		}
		startActivity(intent)
	}

	private fun showError(@StringRes errorString: Int) {
		Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
	}
}
