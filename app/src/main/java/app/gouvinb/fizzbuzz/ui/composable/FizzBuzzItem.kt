package app.gouvinb.fizzbuzz.ui.composable

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import app.gouvinb.fizzbuzz.ui.theme.FizzBuzzTheme

@Composable
fun FizzBuzzItem(text: String) {
	Text(text)
}

@Preview(showBackground = true)
@Composable
fun DefaultFizzBuzzItemPreview() {
	FizzBuzzTheme {
		FizzBuzzItem("42")
	}
}