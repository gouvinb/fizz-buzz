package app.gouvinb.fizzbuzz.ui.composable

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzUi
import app.gouvinb.fizzbuzz.ui.theme.FizzBuzzTheme

@Composable
fun FizzBuzzList(fizzBuzzUi: FizzBuzzUi) {
	LazyColumn(modifier = Modifier.fillMaxSize()) {
		// Remove index 0 (can be optimized)
		items(fizzBuzzUi.rangeLimit - 1) {
			FizzBuzzItem(fizzBuzzUi.parseInt(it + 1))
		}
	}
}

@Preview(showBackground = true)
@Composable
fun DefaultFizzBuzzListPreview() {
	FizzBuzzTheme {
		FizzBuzzList(FizzBuzzUi(2, 4, 15))
	}
}