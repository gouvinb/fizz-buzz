package app.gouvinb.fizzbuzz

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import app.gouvinb.feature.fizzbuzz.ui.model.FizzBuzzUi
import app.gouvinb.fizzbuzz.ui.composable.FizzBuzzList
import app.gouvinb.fizzbuzz.ui.theme.FizzBuzzTheme

class FizzBuzzActivity : ComponentActivity() {
	private lateinit var fizzBuzzUi: FizzBuzzUi

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		(intent?.getSerializableExtra(FIZZ_BUZZ_UI_DATA) as? FizzBuzzUi)?.also { fizzBuzzUi = it } ?: finish()

		setContent {
			FizzBuzzTheme {
				// A surface container using the 'background' color from the theme
				Surface(color = MaterialTheme.colors.background) {
					FizzBuzzList(fizzBuzzUi)
				}
			}
		}
	}

	companion object {
		const val FIZZ_BUZZ_UI_DATA = "FIZZ_BUZZ_UI_DATA"
	}
}
