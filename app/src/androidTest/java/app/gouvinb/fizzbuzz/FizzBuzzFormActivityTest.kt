package app.gouvinb.fizzbuzz

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.Matchers.not
import org.hamcrest.text.IsEmptyString.isEmptyString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.Thread.sleep

@RunWith(AndroidJUnit4::class)
class FizzBuzzFormActivityTest {

	@get:Rule
	var activityScenarioRule = activityScenarioRule<FizzBuzzFormActivity>()

	@Test
	fun validForm() {
		onView(withId(R.id.submit)).check(matches(not(isEnabled())))

		onView(withId(R.id.multiple1_text))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("a"))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("-1"))
			.check(matches(withText("1")))
			.perform(clearText(), typeText("1"))
			.check(matches(withText("1")))

		onView(withId(R.id.submit)).check(matches(not(isEnabled())))

		onView(withId(R.id.multiple2_text))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("a"))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("-1"))
			.check(matches(withText("1")))
			.perform(clearText(), typeText("1"))
			.check(matches(withText("1")))

		onView(withId(R.id.submit)).check(matches(not(isEnabled())))

		onView(withId(R.id.limit_text))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("a"))
			.check(matches(withText(isEmptyString())))
			.perform(typeText("-1"))
			.check(matches(withText("1")))
			.perform(clearText(), typeText("1"))
			.check(matches(withText("1")))

		onView(withId(R.id.submit))
			.check(matches(isEnabled()))
			.perform(click())
	}

	/**
	 * Only for skip form.
	 */
	@Test
	fun benchTest() {
		onView(withId(R.id.submit)).check(matches(not(isEnabled())))

		onView(withId(R.id.multiple1_text))
			.perform(clearText(), typeText("3"))

		onView(withId(R.id.multiple2_text))
			.perform(clearText(), typeText("5"))

		onView(withId(R.id.limit_text))
			.perform(clearText(), typeText(Int.MAX_VALUE.toString()))
			.check(matches(withText(Int.MAX_VALUE.toString())))

		onView(withId(R.id.submit))
			.check(matches(isEnabled()))
			.perform(click())

		if (BuildConfig.DEBUG) sleep(3600_000)
	}
}