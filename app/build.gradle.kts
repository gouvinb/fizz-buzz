import app.gouvinb.gradle.internal.isFromIDE
import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.AndroidBaselicationInfo
import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.Dependencies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("app.gouvinb.build-logic.project-information")
	id("app.gouvinb.build-logic.project-configuration")

	id("com.android.application")
	kotlin("android")
	id("kotlin-android")
}

tasks.withType<KotlinCompile>().all {
	kotlinOptions {
		jvmTarget = AndroidBaselicationInfo.jvmTarget
	}
}

android {
	buildFeatures {
		compose = true
	}
	composeOptions {
		kotlinCompilerExtensionVersion = AndroidBaselicationInfo.composeVersion
	}
}

dependencies {
	// AndroidX
	implementation(Dependencies.androidxAnnotationAnnotation)
	implementation(Dependencies.androidxAppcompatAppcompat)
	implementation(Dependencies.androidxConstraintlayoutConstraintlayout)
	implementation(Dependencies.androidxCoreCoreKtx)
	implementation(Dependencies.androidxLifecycleLifecycleLiveDataKtx)
	implementation(Dependencies.androidxLifecycleLifecycleRuntimeKtx)
	implementation(Dependencies.androidxLifecycleLifecycleViewmodelKtx)

	// Material
	implementation(Dependencies.comGoogleAndroidMaterialMaterial)

	// Compose
	implementation(Dependencies.androidxComposeUiUi)
	implementation(Dependencies.androidxComposeMaterialMaterial)
	implementation(Dependencies.androidxComposeUiUiToolingPreview)
	implementation(Dependencies.androidxActivityActivityCompose)
	when {
		project.isFromIDE() -> implementation(Dependencies.androidxComposeUiUiTooling)
		else -> project.logger.info("androidx.compose.ui:ui-tooling not added to project.")
	}

	// Testing
	testImplementation(Dependencies.junitJunit)
	androidTestImplementation(Dependencies.androidxTestCore)
	androidTestImplementation(Dependencies.androidxTestCoreKtx)
	androidTestImplementation(Dependencies.androidxTestExtJunit)
	androidTestImplementation(Dependencies.androidxTestExtJunitKtx)
	androidTestImplementation(Dependencies.androidxTestEspressoEspressoCore)
	androidTestImplementation(Dependencies.androidxTestRunner)
	androidTestImplementation(Dependencies.androidxComposeUiUiTestJUnit4)
	implementation(project(":feature-fizzbuzz"))
	implementation(project(":library-utils"))

	// Local
}
