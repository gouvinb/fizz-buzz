plugins {
	`java-gradle-plugin`
	`kotlin-dsl`
	kotlin("jvm") version "1.5.31"
}

group = "app.gouvinb.gradle"

repositories {
	google()
	gradlePluginPortal()
	mavenCentral()
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
	kotlinOptions {
		jvmTarget = "11"
	}
}

dependencies {
	implementation(kotlin("stdlib"))
	implementation("com.android.tools.build:gradle:7.0.4")
	implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.31")
	implementation("io.gitlab.arturbosch.detekt:detekt-gradle-plugin:1.19.0")
}

gradlePlugin {
	@Suppress("UNUSED_VARIABLE")
	val projectInformationPlugin by plugins.creating {
		id = "app.gouvinb.build-logic.project-information"
		implementationClass = "app.gouvinb.gradle.plugin.ProjectInformationPlugin"
	}

	@Suppress("UNUSED_VARIABLE")
	val projectConfigurationPlugin by plugins.creating {
		id = "app.gouvinb.build-logic.project-configuration"
		implementationClass = "app.gouvinb.gradle.plugin.ProjectConfigurationPlugin"
	}
}