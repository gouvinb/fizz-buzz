package app.gouvinb.gradle.plugin

import org.gradle.api.JavaVersion
import org.gradle.api.Plugin
import org.gradle.api.Project

class ProjectInformationPlugin : Plugin<Project> {
	override fun apply(target: Project) {}

	@Suppress("unused")
	companion object {
		object AndroidBaselicationInfo {
			const val applicationId = "app.gouvinb.fizzbuzz"
			const val complileSdk = 31
			const val minSdk = 29
			const val targetSdk = 31
			const val versionCode = 1
			const val versionName = "1.0"
			val sourceCompatibility = JavaVersion.VERSION_11
			val targetCompatibility = JavaVersion.VERSION_11
			const val jvmTarget = "11"
			const val composeVersion = "1.0.5"
		}

		object Dependencies {
			const val androidxActivityActivityCompose = "androidx.activity:activity-compose:1.4.0"
			const val androidxAnnotationAnnotation = "androidx.annotation:annotation:1.3.0"
			const val androidxAppcompatAppcompat = "androidx.appcompat:appcompat:1.4.1"
			const val androidxConstraintlayoutConstraintlayout = "androidx.constraintlayout:constraintlayout:2.1.3"
			const val androidxCoreCoreKtx = "androidx.core:core-ktx:1.7.0"

			private const val lifecycleVersion = "2.4.0"
			const val androidxLifecycleLifecycleLiveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycleVersion"
			const val androidxLifecycleLifecycleRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycleVersion"
			const val androidxLifecycleLifecycleViewmodelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleVersion"

			const val comGoogleAndroidMaterialMaterial = "com.google.android.material:material:1.5.0"

			private const val composeVersion = AndroidBaselicationInfo.composeVersion
			const val androidxComposeUiUi = "androidx.compose.ui:ui:$composeVersion"
			const val androidxComposeMaterialMaterial = "androidx.compose.material:material:$composeVersion"
			const val androidxComposeUiUiToolingPreview = "androidx.compose.ui:ui-tooling-preview:$composeVersion"
			const val androidxComposeUiUiTooling = "androidx.compose.ui:ui-tooling:$composeVersion"
			const val androidxComposeUiUiTestJUnit4 = "androidx.compose.ui:ui-test-junit4:$composeVersion"

			const val junitJunit = "junit:junit:4.13.2"

			private const val androidxTestCoreVersion = "1.4.0"
			const val androidxTestCore = "androidx.test:core:$androidxTestCoreVersion"
			const val androidxTestCoreKtx = "androidx.test:core-ktx:$androidxTestCoreVersion"

			private const val androidxTestExtJunitVersion = "1.1.3"
			const val androidxTestExtJunit = "androidx.test.ext:junit:$androidxTestExtJunitVersion"
			const val androidxTestExtJunitKtx = "androidx.test.ext:junit-ktx:$androidxTestExtJunitVersion"

			private const val espressoVersion = "3.4.0"
			const val androidxTestEspressoEspressoCore = "androidx.test.espresso:espresso-core:$espressoVersion"

			const val androidxTestRunner = "androidx.test:runner:1.4.0"
		}
	}
}