package app.gouvinb.gradle.plugin

import app.gouvinb.gradle.internal.configureAndroidLibrary
import app.gouvinb.gradle.internal.configureAndroidLibraryComponents
import app.gouvinb.gradle.internal.configureApplicationAndroidComponents
import app.gouvinb.gradle.internal.configureBase
import app.gouvinb.gradle.internal.configureBaseAppModule
import com.android.build.api.variant.ApplicationAndroidComponentsExtension
import com.android.build.api.variant.LibraryAndroidComponentsExtension
import com.android.build.gradle.LibraryExtension
import com.android.build.gradle.internal.dsl.BaseAppModuleExtension
import com.android.build.gradle.internal.plugins.AppPlugin
import com.android.build.gradle.internal.plugins.LibraryPlugin
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

@Suppress("unused")
class ProjectConfigurationPlugin : Plugin<Project> {
	override fun apply(target: Project) {
		target.apply(block = {
			plugins.all {
				when (this) {
					is AppPlugin -> {
						extensions.getByType<BaseAppModuleExtension>().apply {
							configureBase(project)
							configureBaseAppModule()
						}
						extensions.getByType<ApplicationAndroidComponentsExtension>().configureApplicationAndroidComponents()
					}
					is LibraryPlugin -> {
						extensions.getByType<LibraryExtension>().apply {
							configureBase(project)
							configureAndroidLibrary()
						}
						extensions.getByType<LibraryAndroidComponentsExtension>().configureAndroidLibraryComponents()
					}
					else -> project.logger.debug("IGNORE $project is ${this.javaClass}")
				}
			}
		})
	}
}