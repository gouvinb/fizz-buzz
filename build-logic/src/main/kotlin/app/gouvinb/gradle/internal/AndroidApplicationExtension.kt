package app.gouvinb.gradle.internal

import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.AndroidBaselicationInfo
import com.android.build.api.variant.ApplicationAndroidComponentsExtension
import com.android.build.gradle.internal.dsl.BaseAppModuleExtension

@Suppress("UnstableApiUsage")
fun BaseAppModuleExtension.configureBaseAppModule() {
	defaultConfig {
		applicationId = AndroidBaselicationInfo.applicationId
		minSdk = AndroidBaselicationInfo.minSdk
		targetSdk = AndroidBaselicationInfo.targetSdk
		versionCode = AndroidBaselicationInfo.versionCode
		versionName = AndroidBaselicationInfo.versionName
	}

	buildFeatures {
		viewBinding = true
	}

	val environmentDimension = "environment"
	flavorDimensions.add(environmentDimension)
	productFlavors {
		create("default") {
			isDefault = true
			dimension = environmentDimension
			versionNameSuffix = "-default"
		}
		create("preview") {
			dimension = environmentDimension
			versionNameSuffix = "-preview"
		}
	}

	testBuildType = "alpha"
}

fun ApplicationAndroidComponentsExtension.configureApplicationAndroidComponents() {
	beforeVariants {
		it.enabled = it.buildType != "debug"
	}
}
