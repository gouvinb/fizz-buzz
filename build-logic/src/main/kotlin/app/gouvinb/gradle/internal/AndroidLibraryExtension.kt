package app.gouvinb.gradle.internal

import app.gouvinb.gradle.plugin.ProjectInformationPlugin
import com.android.build.api.variant.LibraryAndroidComponentsExtension
import com.android.build.gradle.LibraryExtension

@Suppress("UnstableApiUsage")
fun LibraryExtension.configureAndroidLibrary() {
	defaultConfig {
		minSdk = ProjectInformationPlugin.Companion.AndroidBaselicationInfo.minSdk
		targetSdk = ProjectInformationPlugin.Companion.AndroidBaselicationInfo.targetSdk
	}
}

fun LibraryAndroidComponentsExtension.configureAndroidLibraryComponents() {
	beforeVariants {
		it.enabled = it.buildType != "debug"
	}
}