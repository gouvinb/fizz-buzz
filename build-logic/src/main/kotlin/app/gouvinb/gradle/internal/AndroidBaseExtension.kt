package app.gouvinb.gradle.internal

import app.gouvinb.gradle.plugin.ProjectInformationPlugin.Companion.AndroidBaselicationInfo
import com.android.build.gradle.BaseExtension
import org.gradle.api.Project
import java.io.File

@Suppress("UnstableApiUsage")
fun BaseExtension.configureBase(project: Project) {
	compileSdkVersion(AndroidBaselicationInfo.complileSdk)

	defaultConfig {
		minSdk = AndroidBaselicationInfo.minSdk
		targetSdk = AndroidBaselicationInfo.targetSdk
		versionCode = AndroidBaselicationInfo.versionCode
		versionName = AndroidBaselicationInfo.versionName

		testApplicationId = "${AndroidBaselicationInfo.applicationId}.test"
		testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

		vectorDrawables {
			useSupportLibrary = true
		}
	}

	signingConfigs {
		// WARN: Don't do this for production !
		maybeCreate("release").apply {
			initWith(getByName("debug"))
		}
	}

	buildTypes {
		maybeCreate("release").apply {
			isDebuggable = project.isDebugEnvironments()
			isDefault = false
			isMinifyEnabled = project.isNotDebugEnvironments()
			matchingFallbacks.add(if (project.isDebugEnvironments()) "debug" else "release")
			signingConfig = signingConfigs.getByName(if (project.isDebugEnvironments()) "debug" else "release")

			proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
		}
		maybeCreate("beta").apply {
			initWith(getByName("release"))
			isDefault = false
		}
		maybeCreate("alpha").apply {
			initWith(getByName("beta"))
			isDefault = true
		}
	}
	compileOptions {
		sourceCompatibility = AndroidBaselicationInfo.sourceCompatibility
		targetCompatibility = AndroidBaselicationInfo.targetCompatibility
	}

	packagingOptions {
		resources {
			excludes += "/META-INF/{AL2.0,LGPL2.1}"
		}
	}

	lintOptions.apply {
		isCheckDependencies = true
		isCheckGeneratedSources = false
		isCheckReleaseBuilds = true
		isIgnoreTestSources = true

		isCheckAllWarnings = false
		isWarningsAsErrors = true

		isAbortOnError = true

		lintConfig = File("${project.rootDir}/config/lint/lint.xml")

		isAbsolutePaths = false

		htmlReport = true
		htmlOutput = File("${project.buildDir}/reports/lint/lint-result.html")

		textReport = true
		textOutput("stdout")

		xmlReport = false
	}

	testOptions.apply {
		unitTests.isReturnDefaultValues = true
		animationsDisabled = true
	}
}
