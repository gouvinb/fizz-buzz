package app.gouvinb.gradle.internal

import org.gradle.api.Project

fun Project.isFromIDE() = this.properties["android.injected.invoked.from.ide"] == "true"
fun Project.isNotFromIDE() = !isFromIDE()

// fun Project.isFromCommitTag() = System.getenv()["CI_COMMIT_TAG"] != null
// fun Project.isNotFromCommitTag() = !isFromCommitTag()

fun Project.isDebugEnvironments() = isFromIDE() // || isNotFromCommitTag()
fun Project.isNotDebugEnvironments() = isNotFromIDE() // && isFromCommitTag()
